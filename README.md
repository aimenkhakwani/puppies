# _Puppy Adoption_

#### _A simple website for puppy adoption, August 2, 2016_

#### By _**Ayana Powell, Aimen Khakwani**_

## Description

_This is a simple webpage for the adoption of puppies! Look at the pups we have available's photos, description, etc._

## Setup/Installation Requirements

* _click download!_
* _And find your new family pup!_


## Known Bugs

All bugs have been fixed. _

## Support and contact details

_contact us through github.com_

## Technologies Used

_HTML, css_

### License

*Copyright*

Copyright (c) 2016 **_Puppies Corp._**
